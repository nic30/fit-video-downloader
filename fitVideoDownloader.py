import lxml.html
import urllib, urllib.request

# setup your login and pass (same as for wis)
# in the if __name__ == "__main__":
# there is a if for filtering videos in this configuration it downloads only
# "democvičení -plátno"

# 
fitLogin = "xlogin00"
fitPass = "xpass"
# base url of subject which you want to download
subjectUrl = "https://video1.fit.vutbr.cz/av/records-categ.php?id=1096" #IFJ

    

class VUTBR_FIT_CAS_authentizer(object):
    logOutUrl = "https://cas.fit.vutbr.cz/logout.cgi"
    loginFormUrl = "https://cas.fit.vutbr.cz/"
    logInUrl = "https://cas.fit.vutbr.cz/cosign.cgi"
    videoGetIdUrl = "https://cas.fit.vutbr.cz/?cosign-FIT-VIDEO1&https://video1.fit.vutbr.cz/av/"
    def __enter__(self):
        self._getCAS_id()
        return self
    
    def __exit__(self, type, value, traceback):
        self.opener.open(self.logOutUrl)
        
    def __init__(self, login, password):
        self.login = login
        self.password = password
        #self._getCAS_id()
         
    def _getCAS_id(self):
        cookieprocessor = urllib.request.HTTPCookieProcessor()
        self.opener = urllib.request.build_opener( cookieprocessor)
        form = self.opener.open(self.loginFormUrl) # get token to cookie jar
        values = { "login": self.login,
                  "password":self.password,
                  "doLogin":"Log In",
                  "required" : "",
                  "ref":"","service": ""
        }
        data = urllib.parse.urlencode(values)
        resp = self.opener.open(self.logInUrl, data.encode('utf-8')) # login via form
        resp = self.opener.open(self.videoGetIdUrl) # login for videoserver
          
def collectVideoLink(opener, baseUrl, encoding = "iso-8859-2" ):
        resp = opener.open(baseUrl)
        txt = resp.read().decode(encoding)
        doc = lxml.html.fromstring(txt) 
        t = doc.xpath("//*/body/table/tr/td/ul")[0]
        links = {}
        for ch in t.getchildren():
            if ch.tag == "a":
                link = ch.attrib["href"]
            if ch.tag == "div":
                text = ch.text_content()
                links[text] = urllib.request.urljoin(baseUrl, link)
        return links

def getDownloadLink(opener, videoUrl, encoding = "iso-8859-2"):
    resp = opener.open(videoUrl)
    txt = resp.read().decode(encoding)
    doc = lxml.html.fromstring(txt)
    btns = doc.xpath("//*/a[@class='button']")
    for btn in btns:
        if btn.text_content() == "stáhnout":
            return  btn.attrib["href"]

def downloadFile(opener, url):
    u = opener.open(url)
    meta = u.info()
    file_name = meta.get_filename()
    file_size = int(meta.get("Content-Length"))
    
    with open(file_name, 'wb') as f:
        print( "Downloading: %s Bytes: %s" % (file_name, file_size))
        
        file_size_dl = 0
        block_sz = 32768
        while True:
            buffer = u.read(block_sz)
            if not buffer:
                break
        
            file_size_dl += len(buffer)
            f.write(buffer)
            status = r"%s [%3.2f%%]" % (file_name, file_size_dl * 100. / file_size)
            print( status)

if __name__ == "__main__":

  
    with VUTBR_FIT_CAS_authentizer(fitLogin, fitPass) as auth:
        links = collectVideoLink(auth.opener, subjectUrl)
        for name in links.keys():
            doDownload = False
            
            if "demonstrační cvičení k projektu - plátno" in name:
                pass
            if "demonstrační cvičení - plátno" in name:
                doDownload = True
            elif "přednáška - plátno" in name:
                pass
            else:
                pass
            
            if doDownload:
                print("downloading:" + name)
                link = getDownloadLink(auth.opener, links[name])
                downloadFile(auth.opener, link)
           
